# MockUp API

This API behaves similarly to the Fuelics API, but it serves static
sensor data. Used mainly for testing purposes.

## How to run

To build the Docker image for MockUp, do the following:

```
docker build -t sc_mockup .
```

To run MockUp on port 5001 do the following:

```
docker run -p 5001:5001 sc_mockup
```

If you don't want to use Docker, install Python3 in your system and
follow the steps shown in the Dockerfile.

## How to access

The API can be accessed by

```
http://localhost:5001/
```

As an example, the call

```
http://localhost:5001/small/messages/u
```

will return the file `sample_data/small/u.json`.


## How to serve your files

Suppose that you have 2 JSON files that contain static sensor data,
namely `w.json` and `u.json`, such that each file contains a list of
messages of type `"w"` and `"u"` respectively.

Place both files in a new directory `./sample_data/MY_NEW_DIR`.

Then, after rebuilding and rerunning your image, the files will 
be served by the following API calls:

```
http://localhost:5001/MY_NEW_DIR/messages/u
http://localhost:5001/MY_NEW_DIR/messages/w
```

## Alternative configuration

In the default configuration, all served message timestamps will be
moved to the previous day. If you want the API to serve the original
timestamps that appear in the files, change `MOVE_TIMESTAMPS` from
`True` to `False` and rebuild your Docker image.




