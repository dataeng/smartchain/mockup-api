import flask, configparser, json, time
from flask import jsonify
import logging

app = flask.Flask(__name__)

logging.basicConfig(level=logging.INFO)

config = configparser.ConfigParser()
config.read('app.ini')

data_dir = config['DEFAULT']['DATA_DIR']

# Update timestamps to refer to the previous date
# use the timestamp of the last message for reference

def moveTimestamps(messages):
  now = int(time.time())
  start_of_previous_day = now - (now % 86400) - 86400
  
  then = int(messages[len(messages)-1]['timestamp'])
  start_of_that_day = then - (then % 86400)
  
  diff = start_of_previous_day - start_of_that_day
  
  app.logger.info("shifting all timestamps by %s seconds", diff)
  
  for message in messages:
    message['timestamp'] = message['timestamp'] + diff
  
  return messages


# Load data from file
# move timestamps if required

def getDataFromFile(file_path):
  f = open(file_path, "r")
  data = json.load(f)
  f.close()
  
  if ('MOVE_TIMESTAMPS' in config['DEFAULT']):
    if (config['DEFAULT']['MOVE_TIMESTAMPS'] == 'True'):
      data = moveTimestamps(data)
  
  return jsonify(data)


# API definitions

@app.route('/', methods=['GET'])
def home():
  return "<h1>MockUp API</h1><p>This site is a prototype API which sample sensor data</p>"

@app.route('/<data>/messages/w', methods=['GET'])
def api_w(data):
  return getDataFromFile(data_dir + "/" + data + "/w.json")

@app.route('/<data>/messages/u', methods=['GET'])
def api_u(data):
  return getDataFromFile(data_dir + "/" + data + "/u.json")

if __name__ == "__main__":
  app.run()

